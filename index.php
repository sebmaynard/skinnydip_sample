<?php

   require 'vendor/autoload.php';

   use Symfony\Component\HttpFoundation\Request;

   $app = new Silex\Application();
   $app['debug'] = true;

   $shopifyClient = new Shopify\Client();
   $shopifyUrl = "URL_HERE";
   $shopifyPassword = "KEY_HERE";
   $shopifyClient->settings(Array("shopUrl" => $shopifyUrl, "X-Shopify-Access-Token" => $shopifyPassword));
   $app["shopify_client"] = $shopifyClient;

   $app->register(new Silex\Provider\TwigServiceProvider(), array(
      'twig.path' => __DIR__.'/views',
   ));

   $app->get('/', function() use ($app) {
      return $app["twig"]->render("layout.twig", array(
         "something" => "interesting"
      ));
   });

   $app->get('/sample.json', function() use($app) {
      $sample = Array(
         Array(
            "key1" => "value1 a",
            "key2" => "value2 a",
            "key3" => "value3 a",
            "key4" => "value4 a"
         ),
         Array(
            "key1" => "value1 b",
            "key2" => "value2 b",
            "key3" => "value3 b",
            "key4" => "value4 b"
         )
      );
      return $app->json($sample);
   });

   $app->get('/products.json', function(Request $request) use($app) {
      $page = intval($request->get("page"));
      if (!$page) $page = 1;

      $params = Array("limit" => 5, "page" => $page);
      $shopifyProducts = $app["shopify_client"]->getProducts($params)["products"];

      $products = [];
      foreach ($shopifyProducts as $product) {
         // return $app->json($product);
         $products[] = Array("title" => $product["title"]);
      }

      return $app->json($products);
   });

   $app->run();
